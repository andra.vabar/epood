<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Stripe\PaymentIntent;
use Stripe\Stripe;

class ShopController extends Controller

{
    public function index()
    {
        return view('index', [
            'products' => Product::with('category')->get()
        ]);
    }

    public function cart()
    {
        $cartSum = collect(session('cart'))->reduce(function ($carry, $item){
            return $carry + ($item->quantity * $item->price);
        });
        return view ('cart', [
            'cart' => session('cart') ?? [],
            'total' => $cartSum
        ]);
    }


    public function cartAdd(Request $request, Product $product)
    {
        $validator = Validator::make($request->all(), [
            'qty' => 'required|numeric|gt:0'
        ])->passes();

        $qty = $validator ? $request->qty : 1;
        $product->quantity = $qty;
        if (session() -> has ('cart.' .$product->id)){
            $cart_product = session('cart.' .$product->id,$product);
            $cart_product ->quantity += $qty;
            session()->put('cart.' .$product->id, $cart_product);
        } else{
            session()->put('cart.' .$product->id, $product);
        }
        return response('Product added to cart');
    }

    public function cartUpdate(Request $request, Product $product)
    {
        $validator = Validator::make($request->all(), [
            'qty' => 'required|numeric|gt:0'
        ])->passes();

       
        if (session() -> has ('cart.' .$product->id) && $validator){
            $cart_product = session('cart.' .$product->id,$product);
            $cart_product ->quantity = $request->qty;
            session()->put('cart.' .$product->id, $cart_product);
        } 
        return response('Product updated');
    }

    public function destroy(Product $product)
    {
        session()->forget('cart.' .$product->id);
        return response('Product deleted');
    }


    public function pay(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email'
        ]);
        $cartSum = collect(session('cart'))->reduce(function($carry, $item){
            return $carry + ($item->quantity * $item->price);
        });
        Stripe::setApiKey(config('services.stripe.sk'));
        $paymentIntent = PaymentIntent::create([
            'amount' => intval($cartSum * 100),
            'currency' => 'eur',
            'payment_method_types' => ['card'],
        ]);
        return [
            'paymentIntent' => $paymentIntent
        ];
    }


    public function success()
    {
        session()->forget('cart');
        return response('success');
    }



}
