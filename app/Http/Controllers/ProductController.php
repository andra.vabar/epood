<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    

    public function index()
    {

        return view('product/index', [
            'products' => Product::with('category')->get()
        ]);
    }

    
    
     public function create()
    {
        return view('product/add',[
            'category' => Category::all()
        ]);
    }

 
   
    public function store(Request $request)
    {


        //validating the fields of the form.
        $request ->validate([
            'name' => 'required',
            'price' => 'required|numeric',
            'category' => 'required',
            'description' => 'required',
            'image' => 'required'
        ]);

        //saving the image to the folder 'images' and getting the root for the saved image in the file-system
        $file = $request->file('image');
        $path = Storage::putFile('images', $file);

        //saving the product in the database
        Product::create([
            'name' => $request->name,
            'price' => $request->price,
            'category_id' => $request->category,
            'description' => $request->description,
            'image' => $path,
        ]);

        //directing the customer back to filling in the form
        return redirect()->back()->with('message', 'Toode salvestatud');
    }

   
    public function show(Product $id)
    {
        //
    }

  
    public function edit($id)
    {
        $product = Product::find($id);
        return view('product/edit', [
            'product' => $product,
            'category' => Category::all(),
        ]);

       
    }

    

    public function update(Request $request)
    {
         $product = Product::find($request -> id);
         $request ->validate([
            'name' => 'required',
            'price' => 'required|numeric',
            'category' => 'required',
            'description' => 'required',
        ]);
        $file = $request->file('image');
        $path = $product -> image;
        if(!empty($file)){
            Storage::delete($product->image);
            $path = Storage::putFile('images', $file);
        }
        $product->update([
            'name' => $request->name,
            'price' => $request->price,
            'category_id' => $request->category,
            'description' => $request->description,
            'image' => $path,
        ]);
            return redirect()->route('product.index');
    }



    public function destroy(Request $request)
    {
        $product = Product::find($request->id);
        Storage::delete($product->image);
        $product->delete();
        return redirect()->back();
    }
}
