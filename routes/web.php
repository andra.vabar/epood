<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ShopController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ShopController::class, 'index']);
Route::get('cart', [ShopController::class, 'cart'])->name('cart.index');
Route::post('/cart-add/{product}', [ShopController::class, 'cartAdd'])->name('cart.add');

Route::post('/cart-update/{product}', [ShopController::class, 'cartUpdate'])->name('cart.update');
Route::post('/cart-delete/{product}', [ShopController::class, 'destroy'])->name('cart.delete');

Route::post('/pay', [ShopController::class, 'pay'])->name('pay');
Route::post('/success', [ShopController::class, 'success'])->name('success');




Route::middleware('auth')->group(function() {
Route::get('/dashboard', [AdminController::class, 'index'])->name('dashboard');
    
    //product
    Route::get('/products', [ProductController::class, 'index'])->name('product.index');
    Route::get('/product-add', [ProductController::class, 'create'])->name('product.add');
    Route::post('/product-submit', [ProductController::class, 'store'])->name('product.submit'); 
    Route::get('/product-edit/{id}', [ProductController::class, 'edit'])->name('product.edit');
    Route::post('/product-update', [ProductController::class, 'update'])->name('product.update');
    Route::post('/product-delete', [ProductController::class, 'destroy'])->name('product.destroy');
    
   

    //category
    Route::get('/category-add', [CategoryController::class, 'create'])->name('category.add');
    Route::post('/category-submit', [CategoryController::class, 'store'])->name('category.submit');
    
});

require __DIR__.'/auth.php';
