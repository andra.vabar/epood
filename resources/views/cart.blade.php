<x-index-layout>
<div class="max-w-screen-xl px-6 lg:px-12 mx-auto w-full h-full mt-16">
    <a class="flex max-w-min flex-nowrap gap-2 items-center" href="/">
        <div class="text-teal-200 font-bold h-4 w-4"><img src="./assets/arrow.png"/></div>
        <p class="text-gray-500 whitespace-nowrap">Back to shopping</p>
    </a>

    <div class="grid lg:grid-cols-2 gap-6 pb-12 mt-12">
      <div class="flex flex-col divide-y-2 gap-4 mt-10 border-b-2 lg:border-0">
          @foreach ($cart as $item)
            <div class="grid grid-cols-3 gap-4 row-auto smigrid-cols-5 xl:grid-cols-6 py-2 place-items-center">
              <img class="row-span-full sm:block h-16 w-16 object-cover aspect-square" src="{{$item->image}}" alt="">
              <div class="flex flex-col">
                  <p class="text-xs Otext-gray-500">{{$item->category->name}}</p>
                  <p class="text-la font-bold">{{$item->name}}</p>
              </div>
              <div class="flex px-6 flex-col">
                <p class="text-xs text-gray-500">Price</p>
                <p class="text-la font-bold">{{$item->price}}€</p>
              </div>
              <div class="flex px-6 justify-center items-center flex-col">
                <p class="text-x text-gray-500">Quantity</p>
                <form class="flex" method="POST" onsubmit="event.preventDefault(); return updateItem (<?php echo($item->id) ?>, new FormData(event.target))">
                    <input name="qty" type="number" class="text-lg w-14 bg-gray-100 border focus:outline-none focus:ring-0 p-0 text-center font-bold" value="{{$item->quantity}}">
                    <button class="text-sm text-grey-500 font-bold Ibg-teal-800 px-1 bg-teal-800 hover:bg-teal-700">update</button>
                </form>
              </div>
            <div class="flex px-6 flex-col">
                <p class="text-xs Otext-gray-500">Total</p>
                <p class="text-la font-bold">{{$item->quantity * $item->price}}€</p>
            </div>
            <div class="flex px-6 flex-col">
                <p class="text-xs text-gray-500">Remove</p>
                <button onclick="event.preventDefault(); return deleteItem(<?php echo($item->id) ?>)" class="leading-tight text bg-teal-800 hover:bg-teal-700">x</button>
            </div>
        </div>
          @endforeach
        </div>
        
        <div class=" max-w-screen-sm w-full mx-auto">
                <h1 class="text-xl text-gray-500 pb-6 font-bold uppercase">Payment details</h1>
                <form class="space-y-4" id="payment-form">
                  <div class="flex flex-col">
                    <label class="font-bold text-gray-500" for="name">Cardholder name</label>
                    <input oninput="clearErrors('name_error')"  name="name" class="w-full rounded focus:outline-none focus:border-gray-200 focus:ring-0 border-2 border-gray-200" type="text">
                    <span class=" text-xs text-red-600" id="name_error"></span>
                  </div>
                  <div class="flex flex-col">
                    <label  class="font-bold text-gray-500" for="email">E-mail</label>
                    <input oninput="clearErrors('email_error')" name="email" class="w-full rounded focus:outline-none focus:border-gray-200 focus:ring-0 border-2 border-gray-200" type="text">
                    <span class=" text-xs text-red-600" id="email_error"></span>
                  </div>
                  <div class="flex items-center w-full">
                    <hr class="w-1/2 ">
                    <p class="px-4 uppercase font-bold text-gray-400 whitespace-nowrap">Card details</p>
                    <hr class="w-1/2">
                  </div>
                  <div class="border-2 border-gray-200 py-2.5 px-2.5 rounded" id="card-element"><!--Stripe.js injects the Card Element--></div>
                      <button class="w-full mt-2 bg-teal-800 hover:bg-teal-700 text-white py-2 rounded-sm" id="submit">
                        <div class="spinner hidden" id="spinner"></div>
                        <span class="text-gray-200 font-bold" id="button-text">Pay {{$total}}€</span>
                      </button>
                      <p id="card-error" role="alert"></p>
                      <p class="result-message hidden">
                        Payment succeeded, see the result in your
                        <a href="" target="_blank">Stripe dashboard.</a> Refresh the page to pay again.
                      </p>
                </form>
        </div>
    </div>  
</div>    
</x-index-layout>

<script src="https://js.stripe.com/v3/"></script>
<script>
    const updateItem = (id, form) => axios.post(`/cart-update/${id}`, form).then(res => window.location.reload())
    const deleteItem = (id) => axios.post(`/cart-delete/${id}`).then(res => window.location.reload())
    
    const stripe = Stripe ("pk_test_51Kgq6UGmunyfBE7Fbeyy5BwyziCBuz3Px0SEJTTbxWYus0zlqlgK4i2sgUSIVMAOw1n6xsSfYbvbTHW02svoWvLe009UhYgF8W")
    const elements = stripe.elements();
    var style = {
      base: {
        color: "#32325d",
        fontFamily: 'Arial, sans-serif',
        fontSmoothing: "antialiased",
        fontSize: "16px",
        "::placeholder": {
          color: "#32325d"
        }
      },
      invalid: {
        fontFamily: 'Arial, sans-serif',
        color: "#fa755a",
        iconColor: "#fa755a"
      }
    };
    const card = elements.create("card", { style: style });
    card.mount("#card-element");
    const form = document.getElementById("payment-form");
    const nameError = document.getElementById('name_error')
    const emailError = document.getElementById('email_error')


    form.addEventListener("submit", async (event) =>  {
      event.preventDefault();
     const formData = new FormData(event.target)
        axios.post('/pay', formData)
          .then((res)=>{
            handleSubmit(res.data.paymentIntent)
        })
        .catch((err) => {
          err.response.data.errors?.name ? nameError.innerText = err.response.data.errors.name[0] : ''
          err.response.data.errors?.email ? emailError.innerText = err?.response.data.errors.email[0] : ''
  });
    });


const handleSubmit = async (pi) => {
  await stripe.confirmCardPayment(pi.client_secret, {
      payment_method: {
        card: card
      }
    })
    .then(res  => {
      if(res.paymentIntent.status === 'succeeded')
      axios.post('/success')
      .then(window.location.replace('/'));
  })

}

const clearErrors = (id) => document.getElementById(id).innerText = ''


</script>