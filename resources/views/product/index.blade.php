<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Products') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                   <a class="py-2 px-3 text-black bg-gray-800 rounded" href="{{route('product.add')}}">Add new</a>
                </div>
                
                <table class="w-full table-fixed">
                    <thead>
                        <tr class="text-center">
                            <th>Image</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th></th>
                        </tr>
                    
                    </thead>
                    <tbody>
                        @foreach ($products as $item)
                        <tr class="text-center">
                            <td class="py-2">
                                <div class="flex justify-center py-2">
                                    <img class="h-14 w-14 object-cover rounded-full" src="{{ $item->image}}" alt="pilt" >
                                </div>
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap"> {{ $item -> name}} </td>
                            <td class="px-6 py-4 whitespace-nowrap"> {{ $item -> category->name}} </td>
                            <td class="px-6 py-4 whitespace-nowrap"> {{ $item -> description}} </td>
                            <td class="px-6 py-4 whitespace-nowrap"> {{ $item -> price}} </td>
                        
                        
                            <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                <a class="bg-yellow-800 text-black rounded" href="{{ route('product.edit', $item->id)}}">Edit</a>
                            </td>
                            <form method="POST" action="{{ route('product.destroy') }}">
                                @csrf
                                <input type="hidden" name="id" value="{{$item->id}}">
                                <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                    <button class="py-1 px-2 bg-red-500 text-black rounded">Delete</button>
                                </td>
                            </form>
                        </tr>
        
                      @endforeach
                      
                    </tbody>
                  </table>

            </div>
        </div>
    </div>


   

</x-app-layout>