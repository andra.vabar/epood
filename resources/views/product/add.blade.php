<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Adding Products') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if (session('message'))
                <div class="text-green-500">{{ session('message') }}</div>
            @endif      

            <form method="POST" action="{{ route('product.submit') }}" enctype="multipart/form-data">
                @csrf
                <div> 
                    <x-label for="name" :value="__('Name')" />
                    <x-input id="" class="block mt-1 w-full" type="text" placeholder="Product name" name="name" :value="old('name')"  autofocus />
                    
                    <x-label for="price" :value="__('Price')" />
                    <x-input id="" class="block mt-1 w-full" type="text" placeholder="Price" name="price" :value="old('price')" autofocus />
                    
                    <select class ="rounded-md shadow-sm border-red-900 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="category" id="">
                        @foreach ($category as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>

                    <div class="mt-1">
                        <label class="text-sm" for="image">Image</label>
                        <input id="" class="block mt-1 w-full" type="file" name="image" />
                        
                        @error('image')
                        <div class="text-red-500 text-sm mt-1">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="mt-1 pb-6 flex flex-col">
                        <label class="text-sm" for="description">Description</label>
                        <textarea class ="rounded-md shadow-sm border-red-900 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" placeholder="Product description" name="description" id="desc" cols="30" rows="5"></textarea>
                        <!--Sisend name = "description"-->
                        @error('description')
                        <div class="text-red-500 text-sm mt-1">{{ $message }}</div>
                        @enderror
                    </div>                

                    <x-button class="ml-3">
                        {{ __('Submit') }}
                    </x-button>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>