@props([
'product'
])

<div class="hover:-translate-y-1 hover:shadow-2xl pb-4 transition rounded shadow-lg inline-flex flex-col w-72">
    <img class="h-60 aspect-[4/3]" src="{{$product->image}}" alt="image">
    <div class="flex justify-between items-center px-4 pt-2">
        <div>
            <p class="font-bold">{{$product->name}}</p>
            <p class="text-sm">{{$product->description}}</p>
        </div>
        <p>{{$product->price}} €</p>
    </div>
    <form method="POST" onsubmit="event.preventDefault(); return submitData(<?php echo ($product->id) ?>, new FormData(event.target))">
        @csrf
    <div class="px-4 flex justify-between items-center">
       <div class="flex items-center text-sm"> 
            <label for="qty">Qty</label>
            <input class="w-20 border-0 focus:outline-none focus:ring-0" name="qty" value="1" type="number">
        </div>
        <button class="hover:bg-teal-700 bg-teal-800 rounded-sm px-3 py-1 tect-white uppercase font-bold text-sm">Buy</button>
    </div>
    </form>
</div>