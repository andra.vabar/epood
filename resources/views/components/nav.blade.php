@props([
    'cart'
    ])


<div class="flex items-center justify-between px-6 bg-opacity-70 h-full w-full">
    <div>
        <h1>Commerce</h1>
        <p>some random quote</p>
    </div>
    <div class="flex items-center gap-4">
        <x-cart-button :cart="$cart"/>
            @if (Route::has('login'))
            @auth
            <a href="{{ url('/dashboard') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Dashboard</a>
            @else
            <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a>

            @if (Route::has('register'))
             <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
            @endif
            @endauth    
            @endif
    </div>
</div>

