@props([
    'name',
    'type' => '',
    'value' => '',
    'placeholder' => '',
    ])

<input id="{{$name}}" placeholder="{{$placeholder}}" type="{{$type}}" name="{{$name}}" value="{{$value}}"
{!! $attributes->merge(['class' => 'rounded-md shadow-sm border-red-900 focus:border-indigo-300 
focus:ring focus:ring-indigo-200 focus:ring-opacity-50']) !!} />
<!--Error message -->
@error($name)
 <div class="text-red-500 text-sm mt-1">{{ $message }}</div>
@enderror
