@props([
'cart'
])

<a class="relative p-2" href="{{route('cart.index')}}">
    <img class="h-6 w-6" src="./assets/cart.png">
    <div class="absolute top-0 right-0 h-5 w-5 rounded-full flex items-center justify-center text-xs bg-teal-800 text-white">
        {{$cart}}
    </div>
</a>