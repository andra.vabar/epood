<x-index-layout>

    <div class="grid mt-4 pb-12 gap-6 grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 place-items-center">
        @foreach ($products as $product)
        <x-product :product="$product"/>
        @endforeach
    </div>
</x-index-layout>

<script>
    const submitData = (e, form) => axios.post(`cart-add/${e}`,form)
        .then(res => {
            window.location.reload()
        })
</script>
